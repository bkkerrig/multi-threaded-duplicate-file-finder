#define _XOPEN_SOURCE 

/*System includes*/

/*Program includes*/
#include "ds/hash_list.h"
#include "mdup.h"
#include "dircrawler.h"

using namespace std;

string hashfile(char *filename){
  	char buffer[BUFFER_SIZE];
	char *hash = NULL;
	FILE *in;
	int i=0;

	hash =  (char *) malloc(HASH_SIZE);
	if(hash == NULL) {printf("There was a problem malloc'ing\n"); exit(-1);}
	sprintf(buffer, "md5sum %s\n", filename);
	in = popen(buffer, "r");
	if(in == NULL){
		printf("Failed to hash file '%s'", filename);
		perror("popen:");
		exit(1);
	}

	while(fgets(buffer, sizeof(buffer)-1, in) != NULL){}
	pclose(in);

	while(buffer[i] != ' '){
		hash[i] = buffer[i];
		i++;
	}
	hash[i] = '\0';

	return std::string(hash);
}

int main(void){

	
	DupeMap *dmap = new DupeMap(HASH_LIST_SIZE);


	sem_init(&thread_count_lock, 0, 1);

	spawn_thread("./files", dmap);

	while(thread_count > 0){sleep(0.001);}

	printf("\nExecution statistics:\n");
	printf("\t%d threads created\n", stat_thread_count);
	printf("\t%d files hashed\n", dmap->numInsertions());
	printf("\t%d directories traversed\n", stat_directories_traversed); 
	printf("\nList of duplicate files (if any). %d threads were created in total.\n\n", stat_thread_count);

	/*TODO print out duplicate files*/
	/*Get rid of C stuffs*/
	
}
