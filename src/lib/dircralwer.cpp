#include "mdup.h"
#include "dircrawler.h"

int thread_count;
sem_t thread_count_lock;
sem_t add;
int stat_thread_count;
int stat_files_hashed;
int stat_directories_traversed;

int isDirectory(const char *path) {
	struct stat statbuf;
	if (stat(path, &statbuf) != 0){
	
	   	return 0;
	}

	return S_ISDIR(statbuf.st_mode);
}

void increment_thread_count(){

	sem_wait(&thread_count_lock);
		thread_count++;
		stat_thread_count++;
	sem_post(&thread_count_lock);
}


void decrement_thread_count(){

	sem_wait(&thread_count_lock);
		thread_count--;
	sem_post(&thread_count_lock);
}


void* thread_func(void *args)
{
	directory_crawler_args *d = (directory_crawler_args *) args;
	create_list_of_files(d->new_path, d->duplicatesMap);
	
}

void spawn_thread(char * path, DupeMap *dmap){
	pthread_t directory_crawler;
	directory_crawler_args *args = (directory_crawler_args *) malloc(sizeof(directory_crawler_args));

		
	strcpy(args->new_path, path);
	args->duplicatesMap = dmap;

	increment_thread_count();
	pthread_create(&directory_crawler, NULL, &thread_func, args);
	
}

void create_list_of_files(char *directory, DupeMap *dmap){

	char current_path[PATH_MAX];
	char *current_file = (char*) malloc(PATH_MAX);

	sprintf(current_path, "%s/", directory);
	DIR *d = (DIR*) malloc(sizeof(DIR *));


	struct dirent *dir;
	d = opendir(directory);

	if (d){
		while ((dir = readdir(d)) != NULL){

			if((strcmp(dir->d_name, ".") != 0) && (strcmp(dir->d_name, "..") != 0)){

				sprintf(current_file, "%s%s", current_path, dir->d_name);

				if(isDirectory(current_file)){
					stat_directories_traversed++;
					char *new_path = (char*) malloc(PATH_MAX);
					sprintf(new_path, "%s%s", current_path, dir->d_name);
					spawn_thread(new_path, dmap);

				} else{
					char *buffer = (char*) malloc(PATH_MAX);
					strcpy(buffer, current_file);
					dmap->addFile(buffer);
				}
			}
		}
		closedir(d);
	  }

	decrement_thread_count();

}

