#include "mdup.h"

FileList::FileList(string h, string fileName) {
	fileNames.push_back(fileName);
	hash = h;
}

string FileList::getHash() {
	return hash;
}

void FileList::addFile(string fileName) {
	fileNames.push_back(fileName);
}


void DupeMap::addFile(string fileName) {
	
	bool found;
	int hashint, index;
	
	sem_wait(&lock);

	//cout << "Inserting(" << fileName << ")" << endl;

	string hash = hashfile((char *) fileName.c_str());
	FileList *f;

	//select a bucket
	hashint = atoi((char*) hash.c_str());
	if(hashint < 0) {
		hashint = 0;
	}


	index = hashint % HASH_LIST_SIZE;
	vector<FileList*> *selectedBucket = &(buckets[index]);

	
	found = false;

	if(selectedBucket->empty()) {
		//cout << "\tNo bucket for " << fileName << "" << endl;
		selectedBucket->push_back(new FileList(hash, fileName));
		
	} else {
		//Look for exisitng file list
		std::vector<FileList*>::iterator it;
		for(it = selectedBucket->begin(); it < selectedBucket->end(); it++) {
			f = *it; /*TODO: hmmm*/
			//cout << "comparing " << f->getHash() << "to" <<  hash << endl;
			if(f->getHash().compare(hash) == 0) {
				cout << "\tFile list found for " << fileName << "" << endl;
				f->addFile(fileName);
				found = true;
			}
		}
		
		if(!found) {
			//cout << "\tNo file list found for " << fileName << "" << endl;
			selectedBucket->push_back(new FileList(hash, fileName));	
		}
	}

	insertionCount++;


	sem_post(&lock);


}

int DupeMap::numInsertions() {
	return insertionCount;
}

DupeMap::DupeMap(int size)
 : HashList<FileList *>(size), insertionCount(0)

{
	sem_init(&lock, 0, 1); /*Only one lock for now*/

}
