#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <linux/limits.h>
#include <string.h>
#include <ftw.h>
#include <semaphore.h>
#include <vector>
#include <string>
#include <functional>
#include <iostream>

#include "ds/hash_list.h"

#ifndef _FINDDUP_H
#define _FINDDUP_H


#define HASH_SIZE 1024
#define BUFFER_SIZE 1024
#define HASH_LIST_SIZE  1000

using namespace std;

class FileList {
	private:
		string hash;
		vector<std::string> fileNames; /*TODO automatic allocation?*/
	public:
		FileList(string hash, string fileName);
		string getHash();
		void addFile(string fileName);
};	

class DupeMap : HashList<FileList *> {
	private:
		sem_t lock;
		int insertionCount;
	public:
		DupeMap(int size);
		void addFile(string);
		int numInsertions();
};

string hashfile(char *filename);

/*Arguments to be passed to directory_crawler threads*/
typedef struct directory_crawler_args{
	char new_path[PATH_MAX];
	DupeMap *duplicatesMap;

} directory_crawler_args;

#endif
