#include <semaphore.h>
#include <string>
#include "mdup.h"

#ifndef _DIRCRAWLER_H
#define _DIRCRAWLER_H

void create_list_of_files(char *directory, DupeMap *dmap);
void spawn_thread(char * path, DupeMap *dmap);
void* thread_func(void *args);
void decrement_thread_count();
void increment_thread_count();

extern int thread_count;
extern sem_t thread_count_lock;
extern sem_t add;
extern int stat_thread_count;
extern int stat_files_hashed;
extern int stat_directories_traversed;

int isDirectory(const char *path);


#endif
