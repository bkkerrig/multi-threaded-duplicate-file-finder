#include <semaphore.h>
#include <vector>


#ifndef _HL_H
#define _HL_H

using namespace std;

template <class T>
class HashList {

	private:

	protected:
		vector< vector<T> > buckets;
	public:
		HashList<T>(int size);

		
};

//https://stackoverflow.com/questions/8752837/undefined-reference-to-template-class-constructor
template<class T>
HashList<T>::HashList(int size) :
 buckets(size)
{

}
#endif
