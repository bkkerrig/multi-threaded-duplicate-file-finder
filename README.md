# README #

### What is this repository for? ###
This is a personal project I am incrementally working on in order to apply knowledge of concurrency, data structures. I'll also be using this project to practice coding standards and deployment practices.


### What is the program? ###
Duplicate file finder: multiple threads (pthreads) traverse a directory tree and utilize a hash map to keep track of duplicate files (md5 hashed). Race conditions are avoided by adding semaphore protection onto each of the nodes in the map's buckets.


### How do I get set up? ###
1) clone
2) make
3) ./finddup (make sure a directory called 'files' exists (main arguments aren't added yet)
