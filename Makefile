TOPDIR := ./
SRCDIR := $(TOPDIR)src/
OBJDIR := $(TOPDIR)obj/
BUILD_DIR := $(TOPDIR)bin/
INCLUDES :=$(SRCDIR)include/
NAME := mdup
EXE := $(BUILD_DIR)$(NAME)

SOURCES := $(shell find $(SRCDIR) -name "*.cpp")#Find all c files
OBJECTS := $(patsubst $(SRCDIR)%.cpp, $(OBJDIR)%.o, $(SOURCES))#For every file in SOURCES, copy the name and replace (SRCDIR & .c) with (OBJDIR & .o)

CC := g++
INCFLAGS = -I$(INCLUDES)
CPPFLAGS :=  $(INCFLAGS)

LINKER := g++
LFLAGS   = -Wall $(INCFLAGS) -lm -pthread

all: $(EXE)
	@echo "Creating "$(EXE)

# $< = first dependency
# $^ = all dependencies
# $@ = target

#Link all compiled objects
$(EXE): $(OBJECTS)
	@echo "Sources: " $(SOURCES)
	@echo "Objects: " $(OBJECTS)
	@echo "Need to link: " $(OBJECTS)
	$(LINKER) $(OBJECTS) $(LFLAGS) -o $@
	@echo "Linking successful!"

#Compile each object, no linking
$(OBJDIR)%.o : $(SRCDIR)%.cpp
	@install -Dv /dev/null $@
	$(CC) $(CPPFLAGS) -c $< -o $@
	@echo "Compiled "$<" successfully!"

run:
	./bin/mdup

clean:
	echo $(OBJECTS)
	@rm -f $(OBJECTS) $(EXE)
